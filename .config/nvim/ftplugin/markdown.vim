" Disable error highlighting when using underscores in latex formulas
autocmd BufNewFile,BufRead,BufEnter *.md :syn match markdownIgnore "\$.*_.*\$"
autocmd BufNewFile,BufRead,BufEnter *.md :syn match markdownIgnore "\$$.*_.*\$$"

