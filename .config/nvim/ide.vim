" TODO merge both files and only use plug plugins lazily.
source $HOME/.config/nvim/init.vim

call plug#begin()
    Plug 'junegunn/fzf'
    Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
call plug#end()

let commands = {
    \'rename': "call CocActionAsync('rename')",
    \'definition': "call CocActionAsync('jumpDefinition')",
    \'type definition': "call CocActionAsync('jumpTypeDefinition')",
    \'usages': "call CocActionAsync('jumpReferences')",
    \'symbols': "CocList symbols",
    \'implementation': "call CocActionAsync('jumpImplementation')",
    \'disable completion': "let b:coc_suggest_disable = 1",
    \'enable completion': "unlet b:coc_suggest_disable",
\}

function ProcessMenuCommand(command)
    execute g:commands[a:command]
endfunction

"let mapleader = " "
nnoremap <ESC> :noh<CR>
nnoremap <C-n> :cnext<CR>
nnoremap <C-p> :cprevious<CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" go-vim configuration
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Disable K opens definition in favour of Coc.
let g:go_doc_keywordprg_enabled = 0

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Coc configuration
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" The autocmd is to override other plugins which might be using the same
" key binding.
nnoremap <silent> K :call ShowDocumentation()<CR>
" Show hover when provider exists, fallback to vim's builtin behavior.
function! ShowDocumentation()
  if CocAction('hasProvider', 'hover')
    call CocActionAsync('definitionHover')
  else
    call feedkeys('K', 'in')
  endif
endfunction
" Go to bindings.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Accept completition using enter.
inoremap <expr> <cr> coc#pum#visible() ? coc#pum#confirm() : "\<CR>"

" Control Space opens completion
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" FZF configuratoin
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Popup window (anchored to the bottom of the current window)
nnoremap <silent> <F5> :call fzf#run({
    \'source': keys(g:commands),
    \'sink': function('ProcessMenuCommand'),
    \'down': '40%'})
    \<CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" General
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Set left column to always appear
set signcolumn=yes

" Disable highlight the 80 column.
autocmd FileType go 2match none
