filetype plugin indent on
syntax enable

" Set line numbers.
set number

set splitbelow
set splitright
set shiftwidth=4
set tabstop=4
set expandtab

" Enable colorschemes, but only if terminal supports so.
if &t_Co > 2 || has("gui_running")
    syntax on
    " Highlight lines that overflow the 80 columns limit
    2match ErrorMsg '\%81v.'
endif

" Mark cursor lines and columns, but only if there are enough colors.
" (These settings don't look so pleasant when there are not enough colors).
"if &t_Co >= 256
"    set cursorline
"endif


" Mark trailing spaces.
if &t_Co >= 2
    " Fancy highlighting for space groups.
    highlight ExtraWhitespace ctermbg=red guibg=red
    match ExtraWhitespace /\s\+$/
else
    " Fallback trailing system.
    set listchars=trail:~
    set list
endif

nnoremap <ESC> :noh<CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Search-related config
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Highlighting.
set hlsearch
" Incremental search.
set incsearch
