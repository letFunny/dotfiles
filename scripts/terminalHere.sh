#!/bin/bash
ID=$(xdpyinfo | grep focus | cut -f4 -d " ")
PID=$(xprop -id $ID | grep -m 1 PID | cut -d " " -f 3)
SHELLPID=$(pgrep -P $PID)

if [ -e "/proc/$SHELLPID/cwd" ]
then
    termite -d $(readlink /proc/$SHELLPID/cwd) &
else
    termite
fi
