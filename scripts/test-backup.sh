#!/bin/bash
if [[ $# -ne 2 ]] ; then
    printf "expect <RESTIC_REPOSITORY>, <dir-to-match>\n";
    exit 1;
fi

set -e

export RESTIC_REPOSITORY="$1"
DIR="$2"
BACKUP_DIR="/mnt/media/b2-test-backup-dir"

# Mount a disk with enough storage.
sudo mount /dev/LVMNasStorage/media /mnt/media
# Create a tmp dir.
mkdir $BACKUP_DIR
restic restore --latest $BACKUP_DIR
diff -r -q $DIR $BACKUP_DIR

notify-send "Tested backup succeded" -a "test-backup.sh"
