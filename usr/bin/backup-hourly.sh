#!/bin/bash
set -e

function on_error {
    echo "Backup job failed"
    notify-send -u critical "Backup job failed" -a "backup-hourly.sh"
    exit 1
}

trap "on_error" ERR INT TERM

# Without explicitly setting the display pinentry fails.
export DISPLAY=":0.0"
USER=$(pass show hetzner/storagebox/albertoPCUsername)
PASS=$(pass show hetzner/storagebox/albertoPCPassword)
SERVER=$(pass show hetzner/storagebox/server)

function sync_dirs {
    # Copy all the files to the remote server, delete files in the remote server
    # that dont exist locally.
    rsync -av --delete -e "sshpass -p $PASS ssh -p 23" $1 $USER@$SERVER:$2
}

# Important, keep the "/" at the end of the first path.
sync_dirs "Documents/" "Documents"
sync_dirs ".password-store/" ".password-store"
notify-send "Backup job succeded" -a "backup-hourly.sh"
