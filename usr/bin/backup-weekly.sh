#!/bin/bash
set -e

function on_error {
    echo "Backup job failed"
    notify-send -u critical "Weekly backup job failed" -a "backup-weekly.sh"
    exit 1
}

trap "on_error" ERR INT TERM

# Without explicitly setting the display pinentry fails.
export DISPLAY=":0.0"
export RESTIC_REPOSITORY=b2:alberto-backups-1234

B2_ACCOUNT_KEY="$(pass show backup/b2_account_key)";
B2_ACCOUNT_ID="$(pass show backup/b2_account_id)";
# Subshell to not export the variables.
(
export B2_ACCOUNT_KEY
export B2_ACCOUNT_ID
restic backup .gnupg .password-store Documents --password-file <(pass show backup/b2_restic_password)
)

notify-send "Backup job succeded" -a "backup-weekly.sh"
